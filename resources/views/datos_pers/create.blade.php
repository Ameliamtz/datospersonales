@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{route('datos_pers.store')}}" method="POST">
            @csrf
            <div class="row justify-content-center form-group">
                <div class="col-md-8">
                    <h1>Crear Un Nuevo Registro</h1>
                    <input type="text" name="nombre" class="form-control">
                    <input type="text" name="app" class="form-control">
                    <input type="text" name="apm" class="form-control">
                    <input type="date" name="fecha_NaC" class="form-control">
                    <input type="submit" value="Guardar" class="btn btn-primary">
                </div>
            </div>
        </form>
    </div>
@endsection
