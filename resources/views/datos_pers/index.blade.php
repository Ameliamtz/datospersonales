@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>Datos Personales</h1>
                <table class="table">
                    <tr>
                        <th>ID</th>
                        <th>NOMBRE</th>
                        <th>APELLIDO PATERNO</th>
                        <th>APELLIDO MATERNO</th>
                        <th>FECHA DE NACIMIENTO</th>
                    </tr>
                    @foreach($datos_pers as $datos)
                        <tr>
                            <td>{{$datos->id}}</td>
                            <td>{{$datos->nombre}}</td>
                            <td>{{$datos->app}}</td>
                            <td>{{$datos->apm}}</td>
                            <td>{{$datos->fecha_NaC}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <form action="{{route('datos_pers.create')}}" method="GET">
                <input type="submit" value="Nuevo" class="btn btn-success">
            </form>
        </div>
    </div>
@endsection
